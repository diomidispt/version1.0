The same way i build my project, the project must be able to do the same thing on GitLab's own machines, but GitLab's machines don't have the credentials I have on my own machine, so I need a way through the configuration to pass this information there or tell the GitLab pipeline where this information is (e.g., AWS Secret Manager or GitLab variables) and the 3 stages of the pipeline are defined in a file. Generally, GitLab has a VM to be able to build the code, it needs to be able to fetch from the GitLab repo, so it needs credentials, assuming at this stage it has the credentials and does a git checkout of the repo and downloads it to its machine. The machine I am referring to is GitLab's I don't see it, it is abstract for me and of course, it has an agent on it.

My GitLab pipeline is in the same environment as my GitLab repo, so they communicate with each other easily and without effort for me.

To build the project, some prerequisites exist; there is a requirements.txt file that defines what my application will need to be built, and since the machine has access to the public internet, there is no need for me to do anything, and it installs these requirements.

The test will run with the commands in the test stage.

The Build will be done with the commands in the build stage through Docker.

For the Deploy stage, it needs network access and permissions to be able to send the files of the GitLab VM to my machine at Digital Ocean, so it needs SSH and also the IP.

In short, the machine where CI happens should speak with the machines that are in the environment, e.g., Digital Ocean, for CD to happen.

The credentials are not taken directly in the file or in the GitLab repo but through variables.


1. So the first step i did was to create a local repo and put inside the code from the developer
2. I added this code to my gitlab repo https://gitlab.com/diomidispt/version1.0/-/tree/master
3. I generated an ssh key with the ssh-keygen command and stored it in C:\Users\tsour\.ssh\id_rsa
4. I inserted the variables REGISTRY_PASS,REGISTRY_USER,SSH_KEY into my gitlab variables
5. I modified the yaml file in order to have my image name and my image tag
6. I run the pipeline, test and build was sucessful (deploy was not)
7. I create a droplet in digital ocean and put its IP (165.232.120.122) in my yaml file
8. Went inside my droplet and installed docker with docker io command
9. I manually runned the commands and managed to have a http://localhost:5000/ that you will see in the printscreens

The only part i did not manage it's to deploy the app from the ci/cd pipeline which is a connectivity issue as per my understanding.